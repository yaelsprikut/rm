<!-- right content column-->
      		<div class="col-md-10" id="content">
            	<div class="panel">
    			<div class="panel-heading" style="background-color:#555;color:#eee;">Personal Information</div>   
              	<div class="panel-body">
                    <div class="container">
                <div class="no-gutter row">           
        <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6">  
            
            <div class="well well-sm">
                <div class="row">
                    <div class="col-sm-6 col-md-4">
                         <img src="http://placehold.it/380x500" alt="" class="img-rounded img-responsive" />
                    </div>
                    <div class="col-sm-6 col-md-8">
                        <!-- Contact Info -->
                        <h4><b>Yael Sprikut</b></h4>
                        <p><small><cite title="San Francisco, USA">Toronto, ON <i class="glyphicon glyphicon-map-marker">
                        </i></cite></small></p>
                        <p>
                        <i class="glyphicon glyphicon-envelope"></i>&nbsp;ysprikut@georgebrown.ca
                        <br /></p>
                        <p><i class="glyphicon glyphicon-book"></i>&nbsp;T127 - Computer Programming Analyst
                        <br /></p>
                        <p><i class="glyphicon glyphicon-gift"></i>&nbsp;June 02, 1988</p>
                        <!-- Split button -->
                        <div class="btn-group">
                            <button type="button" class="btn btn-primary"><i class="glyphicon glyphicon-envelope"></i>&nbsp;Send Message</button>
                        </div>
                    </div>
                </div> 
                <?php include 'profile/userbar.php';?>
                </div>
                <hr>
                  <div class="well"> 
                    <h3>About Me</h3>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod 
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, 
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
                    Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat 
                    nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia 
                    deserunt mollit anim id est laborum.
                  </div>
                    <div> 
                        <h3>Work Experience</h3>
                    </div>
                <hr>
                    <div> 
                        <h3>Research Experience</h3>
                    </div>
                <hr>
                    <div class="well"> 
                    <h3>Monster Tags</h3>
                    #CSS #HTML #PHP #WordPress
                  </div>
            </div><!---end of col-xs-12 col-sm-6 col-md-6--->
            <!---RSS FEED -->
                    <div class="col col-sm-4">
                    <h3>News Feed</h3>    
                    <a href="#"><img src="//placehold.it/300x120/0065A4/eee" class="img-responsive"></a>
                    <div class="text-muted"><small>Aug 15 / John Pierce</small></div>
                    <p>
                    Web design has come a long way since 1999.
                    </p>
                    <hr>
                    <a href="#"><img src="//placehold.it/300x120/0065A4/eee" class="img-responsive"></a>
                    <div class="text-muted"><small>Aug 15 / Wilson Traiker</small></div>
                    <p>
                    The "flat" look was a big trend this year.
                    </p>
                  </div>   
        </div><!---end of row--->
    </div><!---end of no-gutter row--->            
</div><!---end of container--->   
            
             	
            </div><!--/panel-body-->
          </div><!--/panel-->
        </div><!--/end right column-->
